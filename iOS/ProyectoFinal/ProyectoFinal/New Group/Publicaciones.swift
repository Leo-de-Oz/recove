//
//  Publicaciones.swift
//  ProyectoFinal
//
//  Created by MAC01 on 12/06/18.
//  Copyright © 2018 Tecsup. All rights reserved.
//

import Foundation
struct Publicaciones:Decodable{
    let id_usuario:Int
    let descripcion:String
    let fecha:Data
    let tipo:String
    let estado:String
    let categorias:String
    let imagen:String
}
