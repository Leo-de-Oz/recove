//
//  Usuarios.swift
//  ProyectoFinal
//
//  Created by MAC01 on 6/06/18.
//  Copyright © 2018 Tecsup. All rights reserved.
//

import Foundation
struct Usuarios:Decodable{
//    let id_usuario:Int
//    let email:String
//    let nombre:String
//    let password:String
//    let avatar:String
//    let direccion:String
//    let tipo:String
//    let calificacion:String
    
    
    let summary:String
    let icon:String
    let temperature:Double
    
    enum SerializationError:Error {
        case missing(String)
        case invalid(String, Any)
    }
    
    init(json:[String:Any]) throws{
        guard let summary = json["summary"] as? String else {throw SerializationError.missing("sumary is missing")}
        guard let icon = json["icon"] as? String else{throw SerializationError.missing("icon is missing")}
        guard let temperature = json["temperatureMax"] as? Double else {throw SerializationError.missing("temp is missong")}
        self.summary = summary
        self.icon = icon
        self.temperature = temperature
    }
    
}
