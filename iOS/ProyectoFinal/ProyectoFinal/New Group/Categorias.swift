//
//  Categorias.swift
//  ProyectoFinal
//
//  Created by MAC01 on 13/06/18.
//  Copyright © 2018 Tecsup. All rights reserved.
//

import Foundation
struct Categorias:Decodable{
    let nombre:String
    let descripcion:String
    let id_categorias:String
}
