//
//  UsuarioViewController.swift
//  ProyectoFinal
//
//  Created by MAC01 on 5/06/18.
//  Copyright © 2018 Tecsup. All rights reserved.
//

import UIKit

class UsuarioViewController: UIViewController {
    var usuario:Usuarios?
    @IBOutlet weak var viewImage: UIImageView!
    @IBOutlet weak var emailtxt: UITextField!
    @IBOutlet weak var nombretxt: UITextField!
    @IBOutlet weak var passwordtxt: UITextField!
    @IBOutlet weak var direcciontxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        emailtxt.text = usuario!.email
//        nombretxt.text = usuario!.nombre
//        passwordtxt.text = usuario!.password
//        direcciontxt.text = usuario!.direccion
        getData()
   }
    func metodoPUT(ruta:String, datos:[String:Any]){
        let url : URL = URL(string: ruta)!
        var request = URLRequest(url: url)
        let session = URLSession.shared
        request.httpMethod = "PUT"
        let params = datos
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        }catch{}
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = session.dataTask(with: request, completionHandler: {(data,response,error) in
            if (data != nil){
                do{
                    let dict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves)
                    print(dict);
                }catch{}
            }
        })
        task.resume()
    }
    @IBAction func btnGuardar(_ sender: Any) {
        let email = emailtxt.text!
        let nombre = nombretxt.text!
        let password = passwordtxt.text!
        let direccion = direcciontxt.text!
        let datos = ["usuarioId":1, "email": "\(email)", "nombre":"\(nombre)","password":"\(password)", "direccion":"\(direccion)"] as Dictionary<String, Any>
        let ruta = "http://localhost:8000/api/usuarios/"
        metodoPUT(ruta: ruta, datos: datos)
        navigationController?.popViewController(animated: true)
    }
    func getData(){
        let url = NSURL(string: "http://localhost:8000/api/usuarios")
        URLSession.shared.dataTask(with: url! as URL){(data, response, error) in
            if error != nil {
                print(error)
                return
            }
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                print(json)
                let decoder = JSONDecoder()
                let prueba = try decoder.decode(Usuarios.self, from: json as! Data)
                print("Prueba: \(prueba.email)")
//                let decoder = JSONDecoder()
//                let nombre = try decoder.decode(Usuarios.self, from: json as! Data)
//                print(nombre.email)
                
            }catch let jsonError{
                print(jsonError)
            }
            }.resume()
    }
}
