const path = require('path')
const glob = require('glob')
/* const app = electron.app
const BrowserWindow = electron.BrowserWindow */

const { app, BrowserWindow } = require('electron')

var mainWindow = null

app.on('window-all-closed', () => {
    if (process.platform != 'darwin') {
        app.quit()
    }
})

app.on('ready', () => {
    mainWindow = new BrowserWindow({
        width: 1200,
        height: 700
    })

    mainWindow.loadURL(`file://${__dirname}/index.html`)
    mainWindow.on('close', () => {
        mainWindow = null
    })
})