import axios from 'axios';

export const DOMAIN_API = 'https://recove.tecsup.conflux.pe/'

const instance = axios.create({
    baseURL:  DOMAIN_API + 'api',
    headers: {'Access-Control-Allow-Origin': '*'}
});

export default instance;